# ECharts省份地图数据合集

#### 介绍
收录中国各个省份的地图JSON数据

#### 其余数据
如果你想精确到市，县城之类的，请从[http://datav.aliyun.com/tools/atlas/#&lat=30.332329214580188&lng=106.72278672066881&zoom=3.5](http://datav.aliyun.com/tools/atlas/index.html#&lat=30.332329214580188&lng=106.72278672066881&zoom=3.5)来获取数据
